import json
import tweepy
from flask import Flask, redirect, request, session

with open("secrets.json") as secrets:
    credentials = json.load(secrets)

consumer_key = credentials["api_key"]
consumer_secret = credentials["api_secret"]
callback = "http://localhost:5000/callback"
request_token = None

app = Flask(__name__)

app.secret_key = b'secret-thing-_5#y2L"F4Q8z\n\xec]/'


@app.route("/auth")
def auth():
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret, callback)
    url = auth.get_authorization_url()
    session["request_token"] = auth.request_token
    return redirect(url)


@app.route("/callback")
def twitter_callback():
    request_token = session["request_token"]
    del session["request_token"]
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret, callback)
    auth.request_token = request_token
    verifier = request.args.get("oauth_verifier")
    auth.get_access_token(verifier)
    # session["token"] = (auth.access_token, auth.access_token_secret)
    print(auth.access_token, auth.access_token_secret)

    with open("secrets.json", "w") as secrets:
        credentials["access_token"] = auth.access_token
        credentials["access_token_secret"] = auth.access_token_secret
        secrets.write(json.dumps(credentials))
    return redirect("/end")


@app.route("/end")
def end_auth():
    return "Autentikazioa bukatu da"

