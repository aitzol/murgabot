## Twitter autentikazioa Oauth bidez

# instalazioa
- Clonatu edo deskargatu repositorioa 
- Ingurune birtual bat sortzea gomendatzen da (Python 3.8.2 bertsioarekin)
- Exekutatu ```pip install -r requirements.txt```

# Twitter aplikazioa
Api bidezko eragiketak burutzeko beharrezkoa da Twitter aplikazio bat sortzea. Horretarako Twitter garatzaile kontu bat izatea  beharrezkoa da: https://dev.twitter.com

- Behin aplikazioa sortuta kopiatu __api key__ eta __api secret__ balioak __secret.json__ fitxategian. (OHARRA: kode hauek ez publikatzea gomendatzen da)
- Aplikazioaren konfigurazioan __calback url__ eremuan gehitu __http://localhost:5000/callback__ balioa

# Erabiltzailearen kredentzialak

Erabiltzaile baten izenean aritzeko, honen baimen behar da. Baimen hori lortzeko beharrezkoa da Twitter Oauth autentikazio fluxua osatzea. Berau burutzeko Flask aplikazio txiki bat erabiliko dugu.

- Exekutatu klonatutako direktorioan ```flask run``` eta nabigatzaile batean ireki http://localhost:5000/auth helbidea
- Egin login erabili nahi duzun Twitter kontuarekin
- Bukatutakoan __secrets.json__ fitxategian kredentzialak gordeko dira (OHARRA: kredentzial horiekin erabiltzaile hori bazina bezala erabili daiteke Twitter API-a)

# Autentikatutako api eskaerak
Behin aurreko fluxua osatuta autentitatutako twitter api eskaerak egitea posible izango da. Horretarako zure python kodean importatu api objektua

```form txiokatu import api```