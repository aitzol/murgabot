import tweepy
import json

with open("secrets.json") as secrets:
    credentials = json.load(secrets)

print(credentials)

if ("api_key" or "api_secret") not in credentials:
    raise Exception("secret.json fitxategia ez da zuzena")

if ("access_token" or "access_token_secret") not in credentials:
    raise Exception("Arrankatu flask aplikazioa kredentzialak lortzeko")

auth = tweepy.OAuthHandler(credentials["api_key"], credentials["api_secret"])
auth.set_access_token(credentials["access_token"], credentials["access_token_secret"])

api = tweepy.API(auth)

